﻿namespace TestSamJabinaJB.BusinessLogic
{
    /// <summary>
    /// Abstraction of Divisibility Classes
    /// </summary>
    public interface IFizzBuzzHandler
    {
        bool IsDivisible(int number);

        string GetTextForNumber(int input);
    }
}