﻿namespace TestSamJabinaJB.Tests
{
    using NUnit.Framework;
    using System.Linq;
    using TestSamJabinaJB.BusinessLogic;
    [TestFixture]
    public class FizzBuzzModelTest
    {
        [Test]
        public void TestIsDivisibleByFive()
        {
            FizzBuzz fizz = new FizzBuzz();

            var oneToLimit = Enumerable.Range(1, 5);

            var result = fizz.Process(oneToLimit);

            Assert.AreEqual(5, result.Count());
            Assert.AreEqual("Fizz", result.ElementAt(4));
        }

        [Test]
        public void TestIsDivisibleByThree()
        {
            FizzBuzz fizz = new FizzBuzz();
            var oneToLimit = Enumerable.Range(1, 3);

            var result = fizz.Process(oneToLimit);

            Assert.AreEqual(3, result.Count());
            Assert.AreEqual("Buzz", result.ElementAt(2));
        }

        //Multiple of 3 and 5
        [Test]
        public void TestIsDivisibleByThreeAndFive()
        {
            FizzBuzz fizz = new FizzBuzz();
            var oneToLimit = Enumerable.Range(1, 15);

            var result = fizz.Process(oneToLimit);

            Assert.AreEqual(15, result.Count());
            Assert.AreEqual("FizzBuzz", result.ElementAt(14));
        }

        // Number if not divisible by 3 and 5
        [Test]
        public void TestActual()
        {
            FizzBuzz fizz = new FizzBuzz();
            var oneToLimit = Enumerable.Range(1, 6);

            var result = fizz.Process(oneToLimit);

            Assert.AreEqual(result.Count(), 6);
            Assert.AreEqual("1", result.ElementAt(0));
            Assert.AreEqual("2", result.ElementAt(1));
            Assert.AreEqual("4", result.ElementAt(3));
        }
    }
}