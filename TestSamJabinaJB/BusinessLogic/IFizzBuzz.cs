﻿namespace TestSamJabinaJB.BusinessLogic
{
    using System.Collections.Generic;

    /// <summary>
    /// Abstraction of business logic to get the list of numbers from 1 to the number given
    /// </summary>
    public interface IFizzBuzz
    {
        List<string> Process(IEnumerable<int> numbers);
    }
}