﻿namespace TestSamJabinaJB.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// The view model for the Fizz Buzz Operation
    /// </summary>
    /// <summary>
    /// Gets or sets the input number.
    /// </summary>
    public class FizzBuzzModel
    {
        [Required]
        [Range(1, 200)]
        public Int32 Limit
        {
            get;
            set;
        }
    }
}