﻿namespace TestSamJabinaJB.BusinessLogic
{
    using System.Collections.Generic;

    /// <summary>
    /// Abstraction of Formatting data based on date
    /// </summary>

    public interface IFormatHandler
    {
        bool IsDayToFormat();

        List<string> FormatWednesDay(IEnumerable<string> numbers);
    }
}