﻿namespace TestSamJabinaJB.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using TestSamJabinaJB.BusinessLogic;

    /// <summary>
    /// The logic to format based on current date
    /// </summary>

    public class FormatHandler : IFormatHandler
    {
        /// <summary>
        /// Check current date is Wed
        /// </summary>
        /// <returns> True or False</returns>

        public bool IsDayToFormat()
        {
            return (DateTime.Today.DayOfWeek == DayOfWeek.Wednesday);
        }

        /// <summary>
        /// Gets the list of formatted numbers .
        /// </summary>
        /// <param name="numbers"> list of numbers from 1 to the number given.</param>
        /// <returns>Formatted Data</returns>

        public List<string> FormatWednesDay(IEnumerable<string> numbers)
        {
            return numbers.Select<string, string>(s => s.Replace("F", "W").Replace("B", "W")).ToList();
        }
    }
}