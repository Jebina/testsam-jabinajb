﻿namespace TestSamJabinaJB.ControllerFactory
{
    using StructureMap;
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    /// <summary>
    /// Structuremap controller factory
    /// </summary>
    public class StructuremapControllerFactory : DefaultControllerFactory
    {
        /// <summary>
        /// Use StructureMap to construct the controller and all its dependencies.
        /// </summary>
        /// <param name="controllerType">Type of the controller.</param>
        /// <param name="requestContext">request context</param>
        /// <returns>A reference to the controller.</returns>

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            if (controllerType == null) return null;

            return ObjectFactory.GetInstance(controllerType) as IController;
        }
    }
}