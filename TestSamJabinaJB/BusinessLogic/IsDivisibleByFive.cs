﻿namespace TestSamJabinaJB.BusinessLogic
{
    /// <summary>
    /// Fizz buzz Divisibility Logic(Number Divisiable  5)
    /// </summary>
    public class IsDivisibleByFive : IFizzBuzzHandler
    {
        /// <summary>
        /// Check the number is Divisible by Five
        /// </summary>
        /// <param name="number">Input number.</param>
        /// <returns> True or False</returns>

        public bool IsDivisible(int number)
        {
            return (number % 5 == 0 && number % 3 != 0);
        }

        /// <summary>
        /// Gets the message corresponding to the number.
        /// </summary>
        /// <param name="number">Input number.</param>
        /// <returns> Text corresponding to the number </returns>

        public string GetTextForNumber(int number)
        {
            return "Fizz";
        }
    }
}