﻿namespace TestSamJabinaJB.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// The business logic of the FizzBuzz functionality
    /// </summary>
    public class FizzBuzz : IFizzBuzz
    {
        private static IEnumerable<IFizzBuzzHandler> Handler;
        private List<string> output = new List<string>();

        static FizzBuzz()
        {
            Handler = GetHandlers().ToList();
        }

        /// <summary>
        /// Gets the types at runtime IFizzBuzzHandler
        /// </summary>

        private static IEnumerable<IFizzBuzzHandler> GetHandlers()
        {
            var handlers =
                Assembly.GetExecutingAssembly().GetExportedTypes().Where(
                    t => typeof(IFizzBuzzHandler).IsAssignableFrom(t) && t.IsClass)
                    .Select(Activator.CreateInstance)
                    .Cast<IFizzBuzzHandler>();

            return handlers;
        }

        /// <summary>
        /// Process the numbers from 1 to the number given .
        /// </summary>
        /// <param name="numbers"> list of numbers from 1 to the number given.</param>
        /// <returns>Fizz Buzz Data</returns>

        public List<string> Process(IEnumerable<int> numbers)
        {
            numbers.ToList().ForEach(Process);
            return output;
        }

        /// <summary>
        /// Match the number with the handler and set the fizz buzz logic implemented data
        /// </summary>
        /// <param name="numbers"> list of numbers from 1 to the number given.</param>
        /// <returns></returns>

        private void Process(int number)
        {
            var handlers = Handler.Where(h => h.IsDivisible(number));

            handlers.ToList().ForEach(h => output.Add(h.GetTextForNumber(number)));
        }
    }
}