﻿using StructureMap;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using TestSamJabinaJB.BusinessLogic;
using TestSamJabinaJB.ControllerFactory;
using TestSamJabinaJB.Models;

namespace TestSamJabinaJB
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode,
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            setUpConfiguration();
            ControllerBuilder.Current.SetControllerFactory(new StructuremapControllerFactory());
        }

        private void setUpConfiguration()
        {
            ObjectFactory.Initialize(cfg =>
            {
                cfg.For<IFizzBuzz>().Singleton().Use<FizzBuzz>();
                cfg.For<IFormatHandler>().Singleton().Use<FormatHandler>();
            });
        }
    }
}