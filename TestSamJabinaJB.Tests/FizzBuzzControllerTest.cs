﻿namespace TestSamJabinaJB.Tests
{
    using Moq;
    using NUnit.Framework;
    using PagedList;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using TestSamJabinaJB.BusinessLogic;
    using TestSamJabinaJB.Controllers;
    using TestSamJabinaJB.Models;

    [TestFixture]
    internal class fizzbuzzcontrollertest
    {
        [Test]
        public void Should_give_index_page_for_input_on_request()
        {
            //Arrange//

            var repository = new Mock<IFizzBuzz>();
            var handlerrepository = new Mock<IFormatHandler>();

            //Act//
            FizzBuzzController controller = new FizzBuzzController(repository.Object, handlerrepository.Object);
            var result = controller.Index() as ViewResult;

            //Assert//
            Assert.AreEqual("Index", result.ViewName);
        }

        [Test]
        public void Should_onePage_twenty_records_from_GeneratedOutput()
        {
            //Arrange//

            int pageIndex = 1;
            var oneToLimit = Enumerable.Range(1, 20);

            var repository = new Mock<IFizzBuzz>();
            var handlerrepository = new Mock<IFormatHandler>();

            FizzBuzzModel fbuzzobj = new FizzBuzzModel();
            fbuzzobj.Limit = 20;

            repository.Setup(m => m.Process(oneToLimit)).Returns(this.GetProcesslist(oneToLimit));

            //Act//
            FizzBuzzController controller = new FizzBuzzController(repository.Object, handlerrepository.Object);

            var result = controller.GenerateOutput(pageIndex, fbuzzobj) as ViewResult;

            var model = result.ViewData.Model as List<string>;

            //Assert//
            Assert.IsInstanceOf(typeof(PagedList<string>),
            result.ViewData.Model, "Correct ViewModel");

            var lists = result.ViewData.Model as PagedList<string>;
            Assert.AreEqual("GenerateOutput", result.ViewName);
            Assert.AreEqual(20, lists.Count, "Got correct number of data");
            Assert.AreEqual(1, (int)lists.PageCount, "correct page Count");
            Assert.AreEqual(1, (int)lists.PageNumber, "correct  page Number");
        }

        public void Should_secondPage_five_records_from_GeneratedOutput()
        {
            //Arrange//

            int pageIndex = 2;
            var oneToLimit = Enumerable.Range(1, 25);

            var repository = new Mock<IFizzBuzz>();
            var handlerrepository = new Mock<IFormatHandler>();
            FizzBuzzModel obj = new FizzBuzzModel();
            obj.Limit = 25;

            repository.Setup(m => m.Process(oneToLimit)).Returns(GetProcesslist(oneToLimit));

            //Act//
            FizzBuzzController controller = new FizzBuzzController(repository.Object, handlerrepository.Object);

            var result = controller.GenerateOutput(pageIndex, obj) as ViewResult;

            var model = result.ViewData.Model as List<string>;

            //Assert//
            Assert.IsInstanceOf(typeof(PagedList<string>), result.ViewData.Model, "Correct ViewModel");

            var lists = result.ViewData.Model as PagedList<string>;
            Assert.AreEqual("GenerateOutput", result.ViewName);
            Assert.AreEqual(5, lists.Count, "Got correct number of data");
            Assert.AreEqual(1, (int)lists.PageCount, "correct page Count");
            Assert.AreEqual(2, (int)lists.PageNumber, "correct  page Number");
        }

        private List<string> GetProcesslist(IEnumerable<int> numbers)
        {
            List<string> output = new List<string>();
            var allnumbers = from num in numbers
                             select num % 15 == 0 ? "FizzBuzz"
                                 : num % 5 == 0 ? "Buzz"
                                 : num % 3 == 0 ? "Fizz"
                                 : num.ToString();
            foreach (var num in allnumbers)
                output.Add(num);

            return output;
        }
    }
}