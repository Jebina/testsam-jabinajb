﻿namespace TestSamJabinaJB.Controllers
{
    using PagedList;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Web.Mvc;
    using TestSamJabinaJB.BusinessLogic;
    using TestSamJabinaJB.Models;

    [HandleError]
    public class FizzBuzzController : Controller
    {
        private IFizzBuzz _fbuzz;
        private IFormatHandler _formathandler;

        /// <summary>
        /// Initialize new instance of FizzBuzzController
        /// </summary>

        public FizzBuzzController(IFizzBuzz fbuzz, IFormatHandler formathandler)
        {
            _fbuzz = fbuzz;
            _formathandler = formathandler;
        }

        /// <summary>
        /// Action method to render the initial view.
        /// </summary>
        /// <returns>Index View</returns>
        [HttpGet]
        public ViewResult Index()
        {
            return View("Index");
        }

        /// <summary>
        /// Action method to render Data button submit.
        /// </summary>
        /// <returns>FizzBuzzView View</returns>
        public ActionResult GenerateOutput(int? page, FizzBuzzModel PostModel)
        {
            // FizzBuzzModel objData = new FizzBuzzModel() { Limit = PostModel.Limit };
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["itemPerPage"]);

            int pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IEnumerable<string> output = new List<string>();
            IEnumerable<string> formattedoutput = new List<string>();
            ViewResult obj = new ViewResult();

            var oneToLimit = Enumerable.Range(1, PostModel.Limit);
            output = _fbuzz.Process(oneToLimit);

            if (_formathandler.IsDayToFormat())
                output = _formathandler.FormatWednesDay(output);

            return View("GenerateOutput", output.ToPagedList(page ?? 1, pageSize));
        }
    }
}